FROM node:13-stretch
RUN apt-get update && apt-get install -y gzip
RUN mkdir /root/goz-ui
WORKDIR /root/goz-ui
COPY . .
RUN npm install
ENV REACT_APP_GOZ_BASENAME=/ui
RUN npm run build
RUN npm run compress

FROM nginx:latest
RUN mkdir /usr/share/nginx/html/ui
COPY --from=0 /root/goz-ui/build /usr/share/nginx/html/ui
COPY --from=0 /root/goz-ui/nginx-goz.conf /etc/nginx/conf.d/default.conf

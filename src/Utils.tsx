const axios = require('axios');


const getSubjectRole = (subject: string):Promise<number> => {
    return new Promise((resolve, reject) => {
      const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""
      axios.get(rootUrl + '/api/v1.0/subjects/' + subject).then((response: any) => {
            resolve(response.data.role)
      }).catch(() => {
        resolve(0);
      })
    })
  }

  const getRepoRole = (subject: string, repo: string):Promise<number> => {
    return new Promise((resolve, reject) => {
      const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""
      axios.get(rootUrl + '/api/v1.0/repos/' + subject + '/' + repo).then((response: any) => {
            resolve(response.data.role)
      }).catch(() => {
        resolve(0);
      })
    })
  }

  export {getSubjectRole, getRepoRole}
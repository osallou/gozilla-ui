import React, {useState, useEffect} from 'react';
import {
  Redirect
} from "react-router-dom";


import {useUser} from '../user-context'

import queryString from 'query-string'

const axios = require('axios');

const CasCallback = (props: any) => {
  const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""
  const data:any = useUser();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [config, setConfig] = useState({cas: [], url: ''});
  const [casValidated, setCasValidated] = useState(false);

  useEffect(() => {
    axios.get(rootUrl + '/api').then((response: any) => {
        setConfig(response.data)
    })
    let params = queryString.parse(props.location.search)
    let ticket = params.ticket
    data.login({type: 'cas', ticket: ticket, server: props.match.params.server})
    setCasValidated(true)

  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);


  return (
    <div>
    { casValidated && <Redirect to="/"/> }
    { !casValidated && <span>Checking auth....</span>}
    </div>
  )
}

export {CasCallback}

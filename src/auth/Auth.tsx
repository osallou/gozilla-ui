
// import React, { useState } from 'react';


class User {
    id: string;
    authenticated: boolean
    token: any
    constructor() {
        this.id = "";
        this.authenticated = false;
        this.token = null;
    }

    loggedIn() {
      localStorage.setItem("goz-token", this.token);
      localStorage.setItem("goz-user", this.id);
    }

    loggedOut() {
      localStorage.removeItem("goz-token");
      localStorage.removeItem("goz-user");
      this.authenticated = false;
      this.token = null;
      this.id = "";
    }

    isLoggedIn() : boolean {
      if(localStorage.getItem('goz-token') !== null && localStorage.getItem('goz-token') !== "") {
        this.token = localStorage.getItem("goz-token");
        this.id = localStorage.getItem("goz-user") || "";
        this.authenticated = true;
        return true
      }
      return false
    }
}

export default class Auth {
    profile: null;

    constructor() {
        /*
      this.auth0 = new auth0.WebAuth({
        domain: process.env.REACT_APP_AUTH0_DOMAIN,
        audience: `https://${process.env.REACT_APP_AUTH0_DOMAIN}/userinfo`,
        clientID: process.env.REACT_APP_AUTH0_CLIENT_ID,
        redirectUri: 'http://localhost:3000/?callback',
        responseType: 'id_token',
        scope: 'openid profile'
      });
      */
  
      this.handleAuthentication = this.handleAuthentication.bind(this);
      this.signIn = this.signIn.bind(this);
    }
  
    signIn() {
      //this.auth0.authorize();
    }
  
    getProfile() {
      return this.profile;
    }
  
    handleAuthentication() {
      return new Promise((resolve, reject) => {
        /*
        this.auth0.parseHash((err, authResult) => {
          if (err) return reject(err);
          if (!authResult || !authResult.idToken) {
            return reject(err);
          }
  
          this.idToken = authResult.idToken;
          this.profile = authResult.idTokenPayload;
          // set the time that the id token will expire at
          this.expiresAt = authResult.idTokenPayload.exp * 1000;
          */
          resolve();
      });
    }
  }

  export {User}
import React, {useState, useEffect, MouseEvent} from 'react';

import {useUser} from './user-context'
//import useReactRouter from 'use-react-router';

//import {withRouter, RouteComponentProps} from "react-router";
/*
import {
    useLocation, Redirect
  } from "react-router-dom";
*/

//import queryString from 'query-string'
import { Card } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

const axios = require('axios');


const UnauthenticatedApp = (props: any) => {

    const data:any = useUser();
    const [config, setConfig] = useState({cas: [], url: ''});
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""

    const [login, setLogin] = useState("")
    const [pass, setPass] = useState("")
    const [msg, setMsg] = useState("")
    const [logged, setLogged] = useState(false)

    const loginChange = (event:any) => {
      setLogin(event.target.value)
    }

    const passChange = (event:any) => {
      setPass(event.target.value)
    }
    const logNow = (event: MouseEvent<HTMLButtonElement>) => {
      event.preventDefault(); // Let's stop this event.
      event.stopPropagation(); // Really this time.
      data.login({id: login, password: pass}).then((response:any) => {
        setMsg("")
        setLogged(true)
      }).catch((err:any) => {
        console.error('login err', err)
        setMsg("Authentication error")
        //setLogged(false)
      })
    }

    useEffect(() => {
      axios.get(rootUrl + '/api').then((response: any) => {
          setConfig(response.data)
      })
      
    }, [rootUrl]);


    const loginWithCas = (casServer:any) => {
      return () => {
        window.location.assign(`${casServer.LoginURL}?service=${config.url}/callback/cas/${casServer.ID}`)
      }
    }

    // TODO manage redirect on login success
    return (
        <div className="unauth">
          { logged && <Redirect to="/repos"/>}
          { !logged && <div>
            <Card>
              <Card.Header>Login with local account</Card.Header>
                <Card.Body>
                  <form className="form">
                  <label>login</label>
                  <input className="form-control" autoComplete="username" name="login" id="login" onChange={loginChange} value={login}/>
                  <label>password or apikey</label>
                  <input className="form-control" autoComplete="current-password" name="password" id="password" type="password" onChange={passChange} value={pass}/>
                  </form>
                </Card.Body>
                  <Card.Footer>
                    <form>
                    <button className="form-control btn btn-primary" onClick={logNow}>login</button>
                    </form>
                    {msg && <div className="alert alert-warning">{msg}</div>}
                  </Card.Footer>
            </Card>
            <Card>
              <Card.Header>Other</Card.Header>
              <Card.Body>
                  <Card.Text>
                  {config && config.cas.map((server:any) => (
                    <button className="btn btn-secondary" key={server.ID} onClick={loginWithCas(server)}>{server.ID} CAS login</button>
                  ))}
                  </Card.Text>
              </Card.Body>
            </Card>
            </div>}        
            <div>


            </div>
        </div>
    )
}

export default UnauthenticatedApp

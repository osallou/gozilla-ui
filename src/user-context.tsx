import React from 'react'
import {useAuth} from './auth-context'
import {User} from './auth/Auth'

const UserContext = React.createContext(new User());

const UserProvider = (props:any) => (
   <UserContext.Provider value={useAuth()} {...props} />
)
const useUser = () => React.useContext(UserContext)


export {UserProvider, useUser}
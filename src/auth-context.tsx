import React, { useState } from 'react'
import { User } from './auth/Auth'

const axios = require('axios');

const AuthContext = React.createContext(new User())



function AuthProvider(props:any) {
  const [user, setUser] = useState(new User());
  user.isLoggedIn()
  const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""


  axios.interceptors.response.use(function (config: any) {
    return config
  }, function (error: any) {
    if (error.request !== undefined && error.request.status === 401) {
      console.debug('request not authorized, need to login')
      
      if (user.authenticated) {
        user.loggedOut()
        setUser(new User())
      }
    }
    if(error.response === undefined) {error.response={data:{message: 'error'}}}
    return Promise.reject(error)
  })

  axios.interceptors.request.use((config: any) => {
    //let data = useUser();
    if (user && user.token !== null) {
      config.headers.Authorization = "Bearer " + user.token;
      // console.info("add bearer auth token", user.token);
    }
    return config;
  }, (error: any) => {
    console.error("axios error", error);
    return Promise.reject(error);
  });

  let weAreStillWaitingToGetTheUserData = false
  if (weAreStillWaitingToGetTheUserData) {
    return <div>loading...</div>
  }

  const loginCas = (data:any) => {
    return new Promise((resolve, reject) => {
      console.debug("cas login")
      axios.get(rootUrl + `/api/v1.0/user/auth/cas?ticket=${data.ticket}&server=${data.server}`).then((response: any) => {
          //console.log('cas answer', response.data)
          let authUser = new User();
          authUser.authenticated = true;
          authUser.id = response.data.user;
          authUser.token = response.data.token;
          setUser(authUser);
          authUser.loggedIn();
          resolve(authUser)
      }).catch((err:any) => {
        reject(err)
      })

    })
  }
  const login = (authData:any) => {
    return new Promise((resolve, reject) => {


      let authUser = new User();
      if (authUser.isLoggedIn()) {
        setUser(authUser);
        resolve(authUser);
      }

      if(authData === null) {
        reject()
      }

      if (authData && authData.type === 'cas') {
        loginCas(authData).then((resp:any) => {
          resolve(resp);
        }).catch((err:any) => {
          reject(err)
        })
      }

      axios.post(rootUrl + '/api/v1.0/user/auth', {
        login: authData.id,
        password: authData.password
      })
      .then(function (response: any) {
        authUser.authenticated = true;
        authUser.id = response.data.user;
        authUser.token = response.data.token;
        setUser(authUser);
        authUser.loggedIn();
        resolve(authUser);
      })
      .catch(function (error: Error) {
        console.error('login error', error);
        reject(error)
      });
    })
  } // make a login request

  const register = () => {} // register the user
  const logout = () => {
    user.loggedOut()
    setUser(new User())
  } 
  return (
    <AuthContext.Provider value={{user, login, register, logout}} {...props} />
  )
}
const useAuth = () => React.useContext(AuthContext)
export {AuthProvider, useAuth}

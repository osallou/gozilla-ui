import React, { useState, useEffect } from 'react';

import {Row, Col} from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import {useUser} from './user-context';

import {GozSubjects} from './components/GozSubjects'
import {GozRepositories} from './components/GozRepositories'
import {GozPackages} from './components/GozPackages'
import {GozVersions} from './components/GozVersions'
import {GozDetails} from './components/GozDetails'

import './authenticated-app.css'

// import { User } from './auth/Auth';

// const axios = require('axios');


const navSubject = 0;
const navSubjectAdmin = 1;
const navRepository = 2;
const navRepositoryAdmin = 3;
const navPackage = 4;
const navVersion = 5;
const navDetails = 6;


const AuthenticatedApp: React.FC = () => {
    //const user = useUser()
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [data, setData] = useState(useUser())
    const [nav, setNav] = useState(navSubject);
    const [subject, setSubject] = useState({id: null, visibility: 0});
    const [repo, setRepo] = useState({id: null, visibility: 0});
    const [pack, setPack] = useState({id: null});
    const [version, setVersion] = useState({version: null});
    const [expand, setExpand] = useState(false)

    const navTo = (to: number) => {
        return (event: any) => {
            setNav(to)
        }
    }
    const useRepo = (repo: any) => {
        setRepo(repo)
        setNav(navPackage);
    }

    const useSubject = (subject: any) => {
        setSubject(subject)
        setNav(navRepository);
    }

    const usePackage = (pack: any) => {
        setPack(pack)
        setNav(navVersion);
    }    

    const useVersion = (version: any) => {
        setVersion(version)
        setNav(navDetails);
    }  

    /*
    useEffect(() => {
        console.log("something changed", data);
    }, [data]);
    */


    useEffect(() => {
        // Met à jour le titre du document via l’API du navigateur
        if (nav === navSubject) {
            // load nav

        }
    }, [nav]);

    const visibilityIcon = (kind: number) => {
        if (kind === 0) {
            return 'globe'
        } else if (kind === 1) {
            return 'umbrella';
        } else {
            return 'lock';
        }
    }

    const toggleExpand = () => {
        setExpand(!expand)
    }

    return (
        <Row className="auth">
            <Col xs={expand? 3: 1} className="text-left">
                <div className="menuleft">{expand ? <span onClick={navTo(navSubject)}>Subjects</span>: <FontAwesomeIcon title="subjects" icon="home" size="2x" onClick={navTo(navSubject)}/>}</div>
                {subject.id !== null && <div className="menuleft">{expand ? <span className="menuLeft" onClick={navTo(navRepository)}>Repositories</span>: <FontAwesomeIcon title="repositories" icon="sitemap" size="2x" onClick={navTo(navRepository)}/>}</div>}
                {subject.id !== null && repo.id !== null && <div className="menuleft">{expand ? <span className="menuLeft" onClick={navTo(navPackage)}>Packages</span>: <FontAwesomeIcon title="packages" icon="archive" size="2x" onClick={navTo(navPackage)}/>}</div>}
                {subject.id !== null && repo.id !== null && pack.id !== null && <div className="menuleft">{expand ? <span className="menuLeft" onClick={navTo(navVersion)}>Package versions</span>: <FontAwesomeIcon title="code-branch" icon="coffee" size="2x" onClick={navTo(navVersion)}/>}</div>}
                {subject.id !== null && <div className="menuleft">{expand ? <span className="menuLeft" onClick={navTo(navSubjectAdmin)}>Subject admin</span>: <FontAwesomeIcon title="subject admin" icon="folder-open" size="2x" onClick={navTo(navSubjectAdmin)}/>}</div>}
                {subject.id !== null && repo.id !== null && <div className="menuleft">{expand ? <span className="menuLeft" onClick={navTo(navRepositoryAdmin)}>Repository admin</span>: <FontAwesomeIcon title="repo admin" icon="user-shield" size="2x" onClick={navTo(navRepositoryAdmin)}/>}</div>}
                <div className="menuleft" onClick={toggleExpand}>{expand? <FontAwesomeIcon icon="angle-double-left" size="2x"/>: <FontAwesomeIcon icon="angle-double-right" size="2x"/>}</div>
            </Col>
            <Col >
                <Row>
                    <Col >
                    {subject.id !== null && <span>/{subject.id} <FontAwesomeIcon icon={visibilityIcon(subject.visibility)}/></span>}
                    {repo.id !== null && <span>/{repo.id} <FontAwesomeIcon icon={visibilityIcon(repo.visibility)}/></span>}
                    {pack.id !== null && <span>/{pack.id}</span>}
                    {version.version !== null && <span>/{version.version}</span>}
                    </Col>
                </Row>
                <Row>
                <Col xs={11}>
                { nav === navSubject && <GozSubjects className="navcard" onSubject={useSubject}/>}
                { nav === navRepository && <GozRepositories className="navcard" subject={subject} onRepo={useRepo}/>}
                { nav === navPackage && <GozPackages className="navcard" subject={subject} repo={repo} onPackage={usePackage}/>}
                { nav === navVersion && <GozVersions className="navcard" subject={subject} repo={repo} pack={pack} onVersion={useVersion}/>}
                { nav === navDetails && <GozDetails className="navcard" subject={subject} repo={repo} pack={pack} version={version}/>}
                </Col>
                </Row>
            </Col>
        </Row>
    )
}

export {AuthenticatedApp}
import React, {useState, useEffect} from 'react';
import { Row, Col, Card, Badge } from 'react-bootstrap';

// import {useUser} from '../user-context'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom';

const axios = require('axios');

const GozRepositories = ({subject, className}: any) => {
    // const data:any = useUser();
    const [repos, setRepos] = useState([]);
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""

    useEffect(() => {
        //console.log("update subject", subject);
        axios.get(rootUrl + '/api/v1.0/repos/' + subject).then((response: any) => {
            //console.log('repos', response.data)
            setRepos(response.data.repos)
        })
    }, [rootUrl, subject]);

    const visibilityIcon = (kind: number) => {
        if (kind === 0) {
            return 'globe'
        } else if (kind === 1) {
            return 'umbrella';
        } else {
            return 'lock';
        }
    }

    const visibilityType = (kind: number) => {
        if (kind === 0) {
            return 'public'
        } else if (kind === 1) {
            return 'protected';
        } else {
            return 'private';
        }
    }

    return (
        <Row className={className}>
            { repos && repos.map((repo: any) => (
                <Col sm={12} key={repo.id}>
                <Card>
                    <Card.Header><Link to={`/repos/${subject}/${repo.id}`}>{repo.id}</Link> <FontAwesomeIcon title={visibilityType(repo.visibility)} icon={visibilityIcon(repo.visibility)}/></Card.Header>
                    <Card.Body>
                        <Card.Text>{repo.description}{repo.last_updated > 0 &&<Badge variant="light" className="float-right">Updated {(new Date(repo.last_updated*1000)).toDateString()}</Badge>}</Card.Text>
                    </Card.Body>
                </Card>
                </Col>
            ))}
            
        </Row>
    )
}

export {GozRepositories}
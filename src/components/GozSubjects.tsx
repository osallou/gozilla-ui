import React, {useState, useEffect} from 'react';
import {useUser} from '../user-context'
import { Row, Col, Card, Tabs, Tab, Badge } from 'react-bootstrap';
import './GozSubjects.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom';

const axios = require('axios');

const GozSubjects = ({className}: any) => {
    const data:any = useUser();
    const [subjects, setSubjects] = useState([]);
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""


    useEffect(() => {
        //console.log("update subjects", data);
        axios.get(rootUrl + '/api/v1.0/repos' ).then((response: any) => {
            //console.log('repos', response.data)
            setSubjects(response.data.subjects)
        })
    }, [rootUrl, data]);

    const visibilityIcon = (kind: number) => {
        if (kind === 0) {
            return 'globe'
        } else if (kind === 1) {
            return 'umbrella';
        } else {
            return 'lock';
        }
    }

    const visibilityType = (kind: number) => {
        if (kind === 0) {
            return 'public'
        } else if (kind === 1) {
            return 'protected';
        } else {
            return 'private';
        }
    }

    return (
        <Row className={className}>
            <Col sm={12}>
            <Tabs defaultActiveKey="yourprojects" id="projects">
                <Tab eventKey="yourprojects" title="Your projects">
                { subjects.filter((subject:any) => subject.type === 0).map((subject: any) => (
                    <Card key={subject.id}>
                        <Card.Header><Link to={`/repos/${subject.id}`}>{subject.id}</Link> <FontAwesomeIcon title={visibilityType(subject.visibility)} icon={visibilityIcon(subject.visibility)}/></Card.Header>
                        <Card.Body>
                <Card.Text>{subject.description}{subject.last_updated > 0 &&<Badge variant="light" className="float-right">Updated {(new Date(subject.last_updated*1000)).toDateString()}</Badge>}</Card.Text>
                        </Card.Body>
                    </Card>
                ))}
                </Tab>
                <Tab eventKey="yourorgs" title="Your organizations">
                { subjects.filter((subject:any) => subject.type === 1).map((subject: any) => (
                    <Card key={subject.id}>
                        <Card.Header><Link to={`/repos/${subject.id}`}>{subject.id}</Link> <FontAwesomeIcon title={visibilityType(subject.visibility)} icon={visibilityIcon(subject.visibility)}/></Card.Header>
                        <Card.Body>
                            <Card.Text>{subject.description}{subject.last_updated > 0 &&<Badge variant="light" className="float-right">Updated {(new Date(subject.last_updated*1000)).toDateString()}</Badge>}</Card.Text>
                        </Card.Body>
                    </Card>
                ))}
                </Tab>
            </Tabs>
            </Col>
        </Row>
    )
}

export {GozSubjects}
import React, {useState, useEffect} from 'react';
import { Row, Col, Card, Accordion, Button, Form } from 'react-bootstrap';

// import {useUser} from '../user-context'

import "./GozPackages.css";
import { GozMembers } from './GozMembers';

const axios = require('axios');


const GozAdminSubject = ({subject}: any) => {
    // const data:any = useUser();
    const [subjectInfo, setSubjectInfo] = useState<any|null> (null);
    const [err , setErr] = useState("");
    const [msg , setMsg] = useState("");
 
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""


    useEffect(() => {
        axios.get(rootUrl + '/api/v1.0/repos/' + subject).then((response: any) => {
            if (response.data.subject.members == null) {
                response.data.subject.members = []
            }
            setSubjectInfo(response.data.subject)
        })
    }, [rootUrl, subject]);

    const save = () => {
        axios.patch(rootUrl + '/api/v1.0/repos/' + subject, subjectInfo).then(() => {
            setMsg("update done!")
            setErr("")
        }).catch(() => { setErr("an error occured"); setMsg("")})
    }

    const setVisibility = (event: React.FormEvent<HTMLInputElement>) => {
        let s = {...subjectInfo}
        s.visibility = event.currentTarget.value
        setSubjectInfo(s)
    }

    const update = (field: string) => {
        return (event: React.FormEvent<HTMLInputElement>) => {
            let s = {...subjectInfo}
            s[field] = event.currentTarget.value
            setSubjectInfo(s)
        }
    }

    const updateMembers = (memberlist: any[]) => {
        let s = {...subjectInfo}
        s.members = memberlist
        setSubjectInfo(s)
    }

    const profiles = {
        "admin": 10,
        "release manager": 20,
        "developper": 30,
        "guest": 40
    }

    return (
        <Row>
            {subjectInfo &&
            <Col xs={12}>
                {err && <div className="alert alert-warning">{err}</div>}
                {msg && <div className="alert alert-info">{msg}</div>}
                <Card className="navcard">
                    <Card.Header>{subject} [owner: {subjectInfo.owner}]</Card.Header>
                    <Card.Body>
                        <Card.Text>{subjectInfo.description}</Card.Text>
                    </Card.Body>
                </Card>

                <Accordion defaultActiveKey="0">
                    <Card>
                        <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0">
                            General
                        </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="0">
                        <Card.Body>
                        <Form>
                            <Form.Group controlId="subjectForm.Description">
                                <Form.Label>Description</Form.Label>
                                <Form.Control as="textarea" rows="3" type="description" value={subjectInfo.description} onChange={update('description')} />
                            </Form.Group>
                            <Form.Group controlId="subjectForm.visibility">
                                <Form.Label>Visibility</Form.Label>
                                <Form.Control as="select" value={subjectInfo.visibility} onChange={setVisibility}>
                                <option value="0">public</option>
                                <option value="1">protected</option>
                                <option value="2">private</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="subjectForm.save">
                                <Button onClick={save}>Save</Button>
                            </Form.Group>
                        </Form>
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey="1">
                            Members
                        </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="1">
                        <Card.Body>
                            <GozMembers members={subjectInfo.members} profiles={profiles} onChange={updateMembers}/>
                            <Form.Group controlId="subjectForm.save">
                                <Button onClick={save}>Save</Button>
                            </Form.Group>
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Card.Header className="alert alert-warning">
                        <Accordion.Toggle as={Button} variant="link" eventKey="2">
                        Manage
                        </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="2">
                        <Card.Body>
                            manage visibility, remove, ...
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </Col>
            }
        </Row>
    )
}

export {GozAdminSubject}

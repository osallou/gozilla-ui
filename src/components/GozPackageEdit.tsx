import React, {useState, useEffect} from 'react';
import { Row, Col, Card, Button, Form } from 'react-bootstrap';
import { getRepoRole } from '../Utils';
import { Redirect } from 'react-router';

// import {useUser} from '../user-context'

const axios = require('axios');

const GozPackageEdit = ({subject_id, repo_id, pack_id, onChange}: any) => {
    const [pack, setPack] = useState({id: '', subject: '', repo: '', description: '', visibility: 0});
    const [allowed, setAllowed] = useState(false);
    const [err, setErr] = useState("")
    const [create, setCreate] = useState(false);
    const [redirectTo, setRedirectTo] = useState(false);
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""

    /*
    check user has dev profile on repo
    */

    useEffect(() => {
        setRedirectTo(false);
        getRepoRole(subject_id, repo_id).then((role) => {
            if(role >=10 && role<=30) {
                setAllowed(true);
            } else {
                setErr("not enough rights")
            }
            if(!pack_id) { 
                let r = {...pack}
                r.subject = subject_id;
                r.repo = repo_id
                setPack(r)
                setCreate(true);
                return;
            }
            setCreate(false);
            //console.log("update pack", subject_id, repo_id, pack_id);
            axios.get(rootUrl + '/api/v1.0/packages/' + subject_id + "/" + repo_id + "/" + pack_id).then((response: any) => {
                setPack(response.data.package)
            })
        })
        
    }, [rootUrl, pack, subject_id, repo_id, pack_id]);

    const onSave = () => {
        if (create) {
            axios.post(rootUrl + `/api/v1.0/packages/${subject_id}/${repo_id}`, pack).then((response: any) => {
                if(onChange) {
                    onChange(subject_id, repo_id, pack_id);
                }
                setRedirectTo(true)
            }).catch((err: any) => {
                console.error('create err', err)
                setErr(err.response.data.message)
            })
        } else {
            axios.patch(rootUrl + `/api/v1.0/packages/${subject_id}/${repo_id}/${pack_id}`, pack).then((response: any) => {
                if(onChange) {
                    onChange(subject_id, repo_id, pack_id);
                }
                setRedirectTo(true)

            }).catch((err: any) => {
                console.error('err', err)
                setErr(err.response.data.message)
            })
        }
    }

    const updateID = (event: React.FormEvent<HTMLInputElement>) => {
            let s = {...pack}
            s.id = event.currentTarget.value
            setPack(s)
    }

    const updateDesc = (event: React.FormEvent<HTMLInputElement>) => {
        let s = {...pack}
        s.description = event.currentTarget.value
        setPack(s)
    }

    return (
        <Row>
            {redirectTo && <Redirect to={`/repos/${subject_id}/${repo_id}/${pack_id || ""}`}/>}

            {err && <div className="alert alert-warning">{err}</div>}
            { allowed &&
                <Col md={12}>
                <Card >
                    <Card.Header>
                    <Form>
                            <Form.Group as={Row} controlId="subjectForm.ID">
                                <Form.Label column sm={2}>Package ID</Form.Label>
                                <Col sm={10}>
                                <Form.Control disabled={!create} value={pack.id} onChange={updateID} />
                                </Col>
                            </Form.Group>
                    </Form>
                    </Card.Header>
                    <Card.Body>
                        <Form>
                            <Form.Group as={Row} controlId="subjectForm.Subject">
                                <Form.Label column sm={2}>Subject</Form.Label>
                                <Col sm={10}>
                                <Form.Control disabled={true} value={pack.subject} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="subjectForm.Repo">
                                <Form.Label column sm={2}>Repository</Form.Label>
                                <Col sm={10}>
                                <Form.Control disabled={true} value={pack.repo} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="subjectForm.Desc">
                                <Form.Label column sm={2}>Description</Form.Label>
                                <Col sm={10}>
                                <Form.Control as="textarea" rows="3" value={pack.description} onChange={updateDesc} />
                                </Col>
                            </Form.Group>
                        </Form>
                    </Card.Body>
                    <Card.Footer>
                        <Button onClick={onSave}>Save</Button>
                    </Card.Footer>
                </Card>
                </Col>
        }
        </Row>
    )
}

export {GozPackageEdit}
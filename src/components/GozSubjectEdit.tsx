import React, {useState, useEffect} from 'react';
import { Row, Col, Card, Button, Form } from 'react-bootstrap';
import { useUser } from '../user-context';
import { GozMembers } from './GozMembers';
import { getSubjectRole } from '../Utils';
import {fileSize} from 'humanize-plus'
import { Redirect } from 'react-router';

// import {useUser} from '../user-context'


const axios = require('axios');

const GozSubjectEdit = ({subject_id, onChange}: any) => {
    const data:any = useUser();
    const members :any[] = [];
    const [err, setErr] = useState("")
    const [allowed, setAllowed] = useState(false)
    const [subject, setSubject] = useState({id: '', description: '', visibility: 0, type: 0, members: members});
    const [quota, setQuota] = useState(-1)
    const [create, setCreate] = useState(false);
    const [redirectTo, setRedirectTo] = useState(false);
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""
    /*
     check user plan ? to see if can create org ?
     check if already have personal repo?
    */

    useEffect(() => {
        setRedirectTo(false);
        if(!subject_id) {
            // default to user identifier for personal subject
            let obj = {...subject}
            obj.id = data.user.id;
            setSubject(obj)
            setCreate(true);
            setAllowed(true);
            setErr("")
            return;
        }
        setCreate(false);
        getSubjectRole(subject_id).then((role) => {
            if (role>=10 && role<=30) {
                setAllowed(true)
                setErr("")
            } else {
                setErr("not enough rights to edit")
                setAllowed(false)
            }
        })
        //console.log("update subject", subject_id);
        axios.get(rootUrl + '/api/v1.0/repos/' + subject_id).then((response: any) => {
            setSubject(response.data.subject)
            setQuota(response.data.quota)
        })
    // eslint-disable-next-line
    }, [rootUrl, subject, subject_id]);

    const onSave = () => {
        if (create) {
            axios.post(rootUrl + '/api/v1.0/repos', subject).then((response: any) => {
                if(onChange) {
                    onChange(subject_id);
                }
                setRedirectTo(true)
            }).catch((err: any) => {
                console.error('create err', err)
                setErr(err.response.data.message)
            })
        } else {
            axios.patch(rootUrl + '/api/v1.0/repos/' + subject_id, subject).then((response: any) => {
                if(onChange) {
                    onChange(subject_id);
                }
                setRedirectTo(true)

            }).catch((err: any) => {
                console.error('err', err)
                setErr(err.response.data.message)
            })
        }
    }

    const updateID = (event: React.FormEvent<HTMLInputElement>) => {
            let s = {...subject}
            s.id = event.currentTarget.value
            setSubject(s)
    }

    const updateDesc = (event: React.FormEvent<HTMLInputElement>) => {
        let s = {...subject}
        s.description = event.currentTarget.value
        setSubject(s)
    }

    const updateType = (event: React.FormEvent<HTMLInputElement>) => {
        let s = {...subject}
        s.type = parseInt(event.currentTarget.value)
        setSubject(s)
    }

    const updateVisibility = (event: React.FormEvent<HTMLInputElement>) => {
        let s = {...subject}
        s.visibility = parseInt(event.currentTarget.value)
        setSubject(s)
    }

    const updateMembers = (memberlist: any[]) => {
        let s = {...subject}
        s.members = memberlist
        setSubject(s)
    }

    const profiles = {
        "admin": 10,
        "release manager": 20,
        "developper": 30,
        "guest": 40
    }

    return (
        <Row>
        {redirectTo && <Redirect to={'/repos/' + (subject_id || "")}/>}
        { allowed &&
                <Col sm={12}>
                {err && <div className="alert alert-warning">{err}</div>}
                <Card >
                    <Card.Header>
                    <Form>
                            <Form.Group as={Row} controlId="subjectForm.ID">
                                <Form.Label column sm={2}>Subject ID</Form.Label>
                                <Col sm={10}>
                                <Form.Control disabled={!create} value={subject.id} onChange={updateID} />
                                </Col>
                            </Form.Group>
                    </Form>
                    </Card.Header>
                    <Card.Body>
                        <Form>
                            <Form.Group as={Row}>
                                <Form.Label column sm={2}>Usage</Form.Label>
                                <Col sm={10}>
                                <Form.Control disabled={true} value={fileSize(quota)}/>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="subjectForm.Desc">
                                <Form.Label column sm={2}>Description</Form.Label>
                                <Col sm={10}>
                                <Form.Control as="textarea" rows="3" value={subject.description} onChange={updateDesc} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="subjectForm.Visibility">
                                <Form.Label column sm={2}>Visisiblity</Form.Label>
                                <Col sm={10}>
                                <Form.Control as="select" value={`${subject.visibility}`} onChange={updateVisibility}>
                                    <option value="0">public</option>
                                    <option value="1">protected</option>
                                    <option value="2">private</option>
                                </Form.Control>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="subjectForm.Type">
                                <Form.Label column sm={2}>Type</Form.Label>
                                <Col sm={10}>
                                <Form.Control disabled={subject_id ? true: false} as="select" value={`${subject.type}`} onChange={updateType}>
                                    <option value="0">personal</option>
                                    <option value="1">organization</option>
                                </Form.Control>
                                </Col>
                            </Form.Group>
                        </Form>
                        {!create &&
                            <GozMembers members={subject.members} profiles={profiles} onChange={updateMembers}/>
                        }
                    </Card.Body>
                    <Card.Footer>
                        <Button onClick={onSave}>Save</Button>
                    </Card.Footer>
                </Card>
                </Col>
        }
        </Row>
    )
}

export {GozSubjectEdit}
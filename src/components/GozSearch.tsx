import React, {useState, useEffect} from 'react';
//import {useUser} from '../user-context'
import { Row, Col, Card } from 'react-bootstrap';
import './GozSubjects.css';

//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link, useLocation } from 'react-router-dom';


const axios = require('axios');

const GozSearch = () => {

    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""
    const [searchRes, setSearchRes] = useState<any[]>([])
    let location :any = useLocation();

    useEffect(() => {
        let q = location.state ? location.state['q'] : null;
        if(!q) {
            return
        }
        const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""
        axios.get(rootUrl + '/api/v1.0/search', {params: {q: q}}).then((response: any) => {
          //console.log('search result', response.data)
          let elts = [];
          for(let i=0;i<response.data.search.length;i++) {
            let r = response.data.search[i];
            let id = r.subject;
            let path = [r.subject]
            if(r.repo) {
                id = `${id}/${r.repo}`
                path.push(r.repo)
                if(r.package) {
                    id = `${id}/${r.package}`
                    path.push(r.package)
                    if(r.version) {
                        id = `${id}/${r.version}`
                        path.push(r.version)
                    }
                }
            }
            let res = {
                id: id,
                path: path.join('/'),
                description: r.description,
            }
            elts.push(res)
          }
          setSearchRes(elts)
          
        }).catch(() => {
        })
    }, [rootUrl, location]);

    return (
        <Row>
        <Col xs={12}>
            <h3>Search results</h3>
        </Col>    
        <Col xs={12}>
                {searchRes.length===0 && <div>Nothing found...</div>}
                { searchRes && searchRes.map((res, index: any) => (
                    <Card key={index}>
                        <Card.Header><Link to={`/repos/${res.path}`}>{res.id}</Link></Card.Header>
                        <Card.Body>
                            <Card.Text>{res.description}</Card.Text>
                        </Card.Body>
                    </Card>
                ))}
        </Col></Row>
    )
}

export {GozSearch}
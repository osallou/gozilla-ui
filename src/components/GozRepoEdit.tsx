import React, {useState, useEffect} from 'react';
import { Row, Col, Card, Button, Form } from 'react-bootstrap';
import { GozMembers } from './GozMembers';
import { getSubjectRole } from '../Utils';
import { Redirect } from 'react-router';

// import {useUser} from '../user-context'


const axios = require('axios');

const GozRepoEdit = ({subject_id, repo_id, onChange}: any) => {
    const members :any[] = [];
    const [err, setErr] = useState("")
    const [allowed, setAllowed] = useState(false)
    const [repo, setRepo] = useState({id: '', subject: '', description: '', visibility: 0, members: members});
    const [create, setCreate] = useState(false);
    const [redirectTo, setRedirectTo] = useState(false);
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""

    /*
    if id = "" (new) => check user has dev profile on subject
    else check user is repo admin for edition (owner=admin)
    */

    useEffect(() => {
        setRedirectTo(false);
        if(!repo_id) { 
            let r = {...repo}
            r.subject = subject_id;
            setRepo(r)
            setCreate(true);

            getSubjectRole(subject_id).then((role) => {
                if (role>=10 && role<=30) {
                    setAllowed(true)
                    setErr("")
                } else {
                    setErr("not enough rights to create")
                    setAllowed(false)
                }
            })
            return;
        }
        setCreate(false);
        //console.log("update repo", subject_id, repo_id);
        axios.get(rootUrl + '/api/v1.0/repos/' + subject_id + "/" + repo_id).then((response: any) => {
            setRepo(response.data.repo)
            if(response.data.role >= 10 && response.data.role <= 30) {
                setErr("")
                setAllowed(true)
            } else {
                setErr("not enought rights to edit")
                setAllowed(false)
            }
        })
    }, [rootUrl, repo, subject_id, repo_id]);

    const onSave = () => {
        if (create) {
            axios.post(rootUrl + `/api/v1.0/repos/${subject_id}`, repo).then((response: any) => {
                if(onChange) {
                    onChange(subject_id, repo_id);
                }
                setRedirectTo(true)
            }).catch((err: any) => {
                console.error('create err', err)
                setErr(err.response.data.message)
            })
        } else {
            axios.patch(rootUrl + `/api/v1.0/repos/${subject_id}/${repo_id}`, repo).then((response: any) => {
                if(onChange) {
                    onChange(subject_id, repo_id);
                }
                setRedirectTo(true)

            }).catch((err: any) => {
                console.error('err', err)
                setErr(err.response.data.message)
            })
        }
    }

    const updateID = (event: React.FormEvent<HTMLInputElement>) => {
            let s = {...repo}
            s.id = event.currentTarget.value
            setRepo(s)
    }

    const updateDesc = (event: React.FormEvent<HTMLInputElement>) => {
        let s = {...repo}
        s.description = event.currentTarget.value
        setRepo(s)
    }

    const updateVisibility = (event: React.FormEvent<HTMLInputElement>) => {
        let s = {...repo}
        s.visibility = parseInt(event.currentTarget.value)
        setRepo(s)
    }

    const updateMembers = (memberlist: any[]) => {
        let s = {...repo}
        s.members = memberlist
        setRepo(s)
    }

    const profiles = {
        "admin": 10,
        "release manager": 20,
        "developper": 30,
        "guest": 40
    }

    return (
        <Row>
            {err && <div className="alert alert-warning">{err}</div>}
            {redirectTo && <Redirect to={`/repos/${subject_id}/${repo_id || ""}`}/>}

            { allowed &&
                <Col sm={12}>
                <Card >
                    <Card.Header>
                    <Form>
                            <Form.Group as={Row} controlId="subjectForm.ID">
                                <Form.Label column sm={2}>Repository ID</Form.Label>
                                <Col sm={10}>
                                <Form.Control disabled={!create} value={repo.id} onChange={updateID} />
                                </Col>
                            </Form.Group>
                    </Form>
                    </Card.Header>
                    <Card.Body>
                        <Form>
                            <Form.Group as={Row} controlId="subjectForm.ID">
                                <Form.Label column sm={2}>Subject</Form.Label>
                                <Col sm={10}>
                                <Form.Control disabled={true} value={repo.subject} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="subjectForm.Desc">
                                <Form.Label column sm={2}>Description</Form.Label>
                                <Col sm={10}>
                                <Form.Control as="textarea" rows="3" value={repo.description} onChange={updateDesc} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="subjectForm.Visibility">
                                <Form.Label column sm={2}>Visisiblity</Form.Label>
                                <Col sm={10}>
                                <Form.Control as="select" value={`${repo.visibility}`} onChange={updateVisibility}>
                                <option value="0">public</option>
                                <option value="1">protected</option>
                                <option value="2">private</option>
                                </Form.Control>
                                </Col>
                            </Form.Group>
                        </Form>
                        {!create &&
                            <GozMembers members={repo.members} profiles={profiles} onChange={updateMembers}/>
                        }
                    </Card.Body>
                    <Card.Footer>
                        <Button onClick={onSave}>Save</Button>
                    </Card.Footer>
                </Card>
                </Col>
        }
        </Row>
    )
}

export {GozRepoEdit}
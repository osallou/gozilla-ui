import React, {useState, useEffect} from 'react';
import {useUser} from '../user-context'
import { Row, Col, Card, Form } from 'react-bootstrap';
import './GozSubjects.css';

//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
//import { Link } from 'react-router-dom';


const axios = require('axios');

const GozUser = ({className}: any) => {

    const data:any = useUser();
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""
    const  [user, setUser] = useState<any|null>(null)

    useEffect(() => {
        axios.get(rootUrl + '/api/v1.0/user' ).then((response: any) => {
            setUser(response.data.user)
        })
    }, [rootUrl, data]);

    return (
        <Row className="justify-content-md-center"><Col xs={6}>
            { user &&
            <Card className="repo">
                <Card.Header>{user.id}</Card.Header>
                <Card.Body>
                    <Form>
                        <Form.Group as={Row} controlId="userForm.Email">
                                <Form.Label column sm={4}>Mail</Form.Label>
                                <Col sm={8}>
                                <Form.Control value={user.email} disabled/>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="userForm.Apikey">
                                <Form.Label column sm={4}>API Key</Form.Label>
                                <Col sm={8}>
                                <Form.Control value={user.apikey} disabled/>
                                </Col>
                            </Form.Group>
                    </Form>
                </Card.Body>
            </Card>
            }
        </Col></Row>
    )
}

export {GozUser}
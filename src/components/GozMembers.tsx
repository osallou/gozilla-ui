import React, {useState, useEffect} from 'react';
import { Row, Col, Table, Form, Button } from 'react-bootstrap';

//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
//import { setServers } from 'dns';


const GozMembers = ({members, profiles, onChange}: any) => {
    // const data:any = useUser();
    const [member, setMember] = useState({
        user: '',
        profile: 40
    });
    const [err, setErr] = useState("")
    const emptyList : any[] = []
    const [memberList, setMemberList] = useState(emptyList)

    useEffect(() => {
        if (members === null) {
            setMemberList(emptyList);
            return
        }
        let m = []
        for(let i=0;i<members.length;i++) {
            m.push(members[i])
        }
        setMemberList(m);
    }, [members, profiles, emptyList]);

    const updateUser = (event: React.FormEvent<HTMLInputElement>) => {
            let s = {...member}
            s.user = event.currentTarget.value
            setMember(s)
    }

    const updateProfile = (event: React.FormEvent<HTMLInputElement>) => {
        let s = {...member}
        s.profile = parseInt(event.currentTarget.value)
        setMember(s)
}

    
    const updateMemberProfile = (member:any) => {
        return function(event: React.FormEvent<HTMLInputElement>) {
            let m = [...memberList]
            for(let i=0;i < m.length; i++) {
                if (m[i].user === member.user) {
                    m[i].profile = parseInt(event.currentTarget.value)
                    setMemberList(m)
                    break
                }
            }
        }

    }
    

    const add = () => {
        let m = [...memberList]
        for(let i=0;i<m.length;i++) {
            if (m[i].user === member.user) {
                setErr("user already exists")
                return
            }
        }
        m.push(member);
        setMemberList(m)
        onChange(m)

        setMember({user:'', profile: 40})
        setErr("")
    }

    return (
        <Row >
            {err && <Col className="alert alert-warning">{err}</Col>}
            <Col>
                <Table>
                    <thead><tr><th>User</th><th>Profile</th><th></th></tr></thead>
                    <tbody>
                    { members && members.map((member:any) => (
                        <tr key={member.user}>
                            <td>{member.user}</td>
                            <td>
                            <Form.Control as="select" value={member.profile.toString()} onChange={updateMemberProfile(member)}>
                            {Object.keys(profiles).map((profile) => (
                                 <option key={profile} value={profiles[profile]}>{profile}</option>
                            ))}
                            </Form.Control>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </Col>
            <Col>
                <Form>
                    <Form.Group controlId="member.id">
                        <Form.Label>User id</Form.Label>
                        <Form.Control value={member.user} onChange={updateUser} />
                    </Form.Group>
                    <Form.Group controlId="member.profile">
                        <Form.Label>Profile</Form.Label>
                        <Form.Control as="select" value={member.profile.toString()} onChange={updateProfile}>
                            {Object.keys(profiles).map((profile) => (
                                 <option key={profile} value={profiles[profile]}>{profile}</option>
                            ))}
                        </Form.Control>
                        <Form.Group controlId="subjectForm.add">
                                <Button onClick={add}>Add</Button>
                            </Form.Group>
                    </Form.Group>
                </Form>
            </Col>
        </Row>
    )
}

export {GozMembers}

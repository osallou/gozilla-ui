import React, {useState, useEffect} from 'react';
import { Row, Col, Card, Badge } from 'react-bootstrap';

// import {useUser} from '../user-context'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom';


const axios = require('axios');

const GozVersions = ({subject, repo, pack, className}: any) => {
    // const data:any = useUser();
    const [versions, setVersions] = useState([]);
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""


    useEffect(() => {
        //console.log("update versions", subject, repo, pack);
        axios.get(rootUrl + '/api/v1.0/packages/' + subject + "/" + repo + "/" + pack + "/versions").then((response: any) => {
            //console.log('versions', response.data)
            setVersions(response.data.versions)
        })
    }, [rootUrl, subject, repo, pack]);


    const packTypeIcon = () => {
      if (pack.type === 0) {
        return "creative-commons-remix"
      }
      if (pack.type === 1) {
        return "java"
      }
      if (pack.type === 2) {
        return "ubuntu"
      }
      if (pack.type === 3) {
        return "fedora"
      }
      if (pack.type === 4) {
        return "docker"
      }
      return "creative-commons-remix"
    }

    return (
        <Row className={className}>
                { versions.map((version: any) => (
                    <Col sm={12} md={6} key={version.version}>
                    <Card className="repo">
                        <Card.Header><Link to={`/repos/${subject}/${repo}/${pack}/${version.version}`}>{version.version}</Link> {(pack.type === 0 || pack.type === 5) ? <FontAwesomeIcon title="archive" icon="archive"/> : <FontAwesomeIcon title={packTypeIcon()} icon={['fab', packTypeIcon()]}/>}</Card.Header>
                        <Card.Body>
                            <Card.Text>{version.description}{version.last_updated > 0 &&<Badge variant="light" className="float-right">Updated {(new Date(version.last_updated*1000)).toDateString()}</Badge>}</Card.Text>
                        </Card.Body>
                    </Card>
                    </Col>
                ))}

        </Row>
    )
}

export {GozVersions}

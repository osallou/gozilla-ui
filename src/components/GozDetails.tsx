import React, {useState, useEffect} from 'react';
import { Row, Col } from 'react-bootstrap';

// import {useUser} from '../user-context'

// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import FileBrowser, { Icons } from 'react-keyed-file-browser'
import Moment from 'moment'
import '../../node_modules/react-keyed-file-browser/dist/react-keyed-file-browser.css'
const axios = require('axios');

const GozDetails = ({subject, repo, pack, version, className}: any) => {
    // const data:any = useUser();
    const [files, setFiles] = useState(new Array(0));
    const [pfiles, setPFiles] = useState<any|null>(null)
    const [downloadUrl, setDownloadUrl] = useState("")
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""

    useEffect(() => {
        //console.log("update files", subject, repo, pack, version);
        axios.get(rootUrl + '/api/v1.0/packages/' + subject + "/" + repo + "/" + pack + "/versions/" + version).then((response: any) => {
            //console.log('files', response.data)
            let packFiles = []
            let details: any = {}
            for(let i=0; i<response.data.files.length;i++) {
                let curfile = response.data.files[i];
                if (curfile.PackageType === 5) {
                    // revisions not supported for now
                    let rrev = "0"
                    let prev = "0"
                    // conan
                    let name = `${rrev}/export/${curfile.Name}`
                    if (curfile.Extras && curfile.Extras.packid) {
                        name = `${rrev}/package/${curfile.Extras.packid}/${prev}/${curfile.Name}` 
                    }
                    curfile.Name = name;
                }
                packFiles.push({
                    key: curfile.Name,
                    size: curfile.Meta.size,
                    modified: Moment(curfile.Meta.last_updated*1000),
                    url: curfile.Path,

                })
                details[curfile.Name] = {
                    'meta': curfile.Meta,
                    'extras': curfile.Extras,
                    'type': curfile.PackageType
                }
            }
            //console.log("packfiles", packFiles)
            setFiles(packFiles)
            setPFiles(details)
        })
    }, [subject, repo, pack, version, rootUrl]);

    const download = (file: string) => {
        return (event: any) => {
        if (pfiles === null) {
            return
        }
        let pfile = pfiles[file];
        let baseUrl = `/dl/${subject}/${repo}/raw/${pack}/${version}/`
        if (pfile.type === 1) {
            baseUrl = `/dl/${subject}/${repo}/maven/`
        } else if (pfile.type === 2) {
            baseUrl = `/dl/${subject}/${repo}/debian/`
        } else if (pfile.type === 3) {
            baseUrl = `/dl/${subject}/${repo}/rpm/`
        } else if (pfile.type === 5) {
            baseUrl = `/dl/${subject}/${repo}/conan/v1/files/${pack}/${version}/`
        }
        //setDownloadUrl(rootUrl + baseUrl + file)
        axios.get(rootUrl + baseUrl + file, {maxRedirects: 0, params: {noredirect: 1}}).then((response: any) => {
            //console.log('get url', response.data)
            setDownloadUrl(response.data.location)
        });

    }
        
    }

    const showDetails = (data: any) => {
        return (
            <div>
                <table className="table table-striped">
                <tbody>
                    <tr><td>Name</td><td>{data.file.key}</td></tr>
                {pfiles && Object.keys(pfiles[data.file.key]['meta']).map((key) => (
                    <tr key={key}>
                        <td>{key}</td><td>{pfiles[data.file.key]['meta'][key]}</td>
                    </tr>
                ))}
                {Object.keys(pfiles[data.file.key]['extras']).map((key) => (
                    <tr key={key}>
                        <td>{key}</td><td>{pfiles[data.file.key]['extras'][key]}</td>
                    </tr>
                ))}
                <tr><td>Download</td><td><small>{!downloadUrl && <button onClick={download(data.file.key)}>Get download link</button>}{downloadUrl && <a href={downloadUrl} target="_blank" rel="noopener noreferrer">{downloadUrl}</a>}</small></td></tr>
                </tbody>
                </table>
            </div>)
    }

    const reset = () => {
        setDownloadUrl("")
    }

    return (
        <Row className={className}>
            <FileBrowser
                files={files}
                icons={Icons.FontAwesome(4)}
                detailRenderer={showDetails}
                onSelectFile={reset}
                  />



                { files.map((file: any) => (
                    <Col md={6} key={file.key}>
                    {file.Path}
                    </Col>
                ))}

        </Row>
    )
}

export {GozDetails}
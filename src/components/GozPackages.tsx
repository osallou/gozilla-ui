import React, {useState, useEffect} from 'react';
import { Row, Col, Card, Tabs, Tab, Badge } from 'react-bootstrap';

// import {useUser} from '../user-context'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import "./GozPackages.css";
import { Link } from 'react-router-dom';
import { useUser } from '../user-context';

const axios = require('axios');

const GozPackages = ({subject, repo, className}: any) => {
    const data:any = useUser();
    const [packages, setPackages] = useState([]);
    const [selectedRepo, setSelectedRepo] = useState<any|null>(null)
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""


    useEffect(() => {
        //console.log("update packages", subject, repo);
        axios.get(rootUrl + '/api/v1.0/packages/' + subject + "/" + repo).then((response: any) => {
            //console.log('packages', response.data)
            setPackages(response.data.packages)
            setSelectedRepo(response.data.repo)
        })
    }, [rootUrl, subject, repo]);

    const packTypeIcon = (pack: any) => {
        if (pack.type === 0) {
          return "archive"
        }
        if (pack.type === 1) {
          return "java"
        }
        if (pack.type === 2) {
          return  "ubuntu"
        }
        if (pack.type === 3) {
          return "fedora"
        }
        if (pack.type === 4) {
          return "docker"
        }
        return "archive"

    }

    return (
        <Row className={className}>
            <Col xs={12}>
            <Row>
                { packages.map((pack: any) => (
                    <Col sm={12} key={pack.id}>
                    <Card className="navcard">
                        <Card.Header><Link to={`/repos/${subject}/${repo}/${pack.id}`}>{pack.id}</Link> {(pack.type ===0 || pack.type === 5) ? <FontAwesomeIcon title="archive" icon="archive"/> : <FontAwesomeIcon title={packTypeIcon(pack)} icon={['fab', packTypeIcon(pack)]}/>}</Card.Header>
                        <Card.Body>
                            <Card.Text>{pack.description}{pack.last_updated > 0 &&<Badge variant="light" className="float-right">Updated {(new Date(pack.last_updated*1000)).toDateString()}</Badge>}</Card.Text>
                        </Card.Body>
                    </Card>
                    </Col>
                ))}
            </Row>
            </Col>
            <Col xs={12}>
            <Tabs defaultActiveKey="download" id="repository_access">
            <Tab eventKey="download" title="Download">
                <Card bg="dark" text="white" className="navcard">
                    <Card.Header>Download</Card.Header>
                        <Card.Body>
                            {selectedRepo && selectedRepo.visibility === 0 &&
                            <Card.Text>
                                <code>curl -O https://{window.location.hostname}/dl/{subject}/{repo}/<i>package</i>/<i>package_version</i>/<i>file_name</i></code>
                            </Card.Text>
                            }
                            {data && data.user.authenticated && selectedRepo && selectedRepo.visibility > 0 &&
                            <Card.Text>
                                <code>curl -H "X-GOZ-USER: {data.user.id}" -H "X-GOZ-APIKEY: <i>API_KEY</i>" -O https://{window.location.hostname}/dl/{subject}/{repo}/raw/<i>package</i>/<i>package_version</i>/<i>file_name</i></code>
                            </Card.Text>
                            }
                        </Card.Body>
                </Card>
            </Tab>
            {data && data.user.authenticated && <Tab eventKey="upload" title="Upload">
                <Card bg="dark" text="white" className="navcard">
                    <Card.Header>Upload</Card.Header>
                        <Card.Body>
                            <Card.Text>
                                <code>curl -L -H "X-GOZ-USER: {data.user.id}" -H "X-GOZ-APIKEY: <i>API_KEY</i>" -X PUT -F file=@<i>local_file_path</i> https://{window.location.hostname}/api/v1.0/content/{subject}/{repo}/<i>package</i>/<i>package_version</i>/<i>remote_file_name</i></code>
                            </Card.Text>
                        </Card.Body>
                </Card>
            </Tab>}
            <Tab eventKey="debian" title="Debian" className="navcard">
                <Card bg="dark" text="white" className="navcard">
                    <Card.Header>Debian repo</Card.Header>
                        <Card.Body>
                            <div>Add to /etc/apt/sources.list.d/{subject}_{repo}.list:</div>
                            <Card.Text>
                                <code>deb https://{window.location.hostname}/dl/{subject}/{repo}/debian <i>distribution</i> main</code>
                            </Card.Text>
                        </Card.Body>
                </Card>
            </Tab>
            <Tab eventKey="rpm" title="Rpm" className="navcard">
                <Card bg="dark" text="white" className="navcard">
                    <Card.Header>Rpm repo</Card.Header>
                        <Card.Body>
                            <Card.Text className="codeBlock">
                            <code>[home_{subject}_{repo}]</code><br/>
                            <code>name={subject}_{repo}</code><br/>
                            <code>type=rpm-md</code><br/>
                            <code>baseurl=https://{window.location.hostname}/dl/{subject}/{repo}/rpm/<i>distribution</i>>/</code><br/>
                            <code>gpgcheck=1</code><br/>
                            <code>gpgkey=https://{window.location.hostname}/dl/gpg</code><br/>
                            <code>enabled=1</code>
                            </Card.Text>
                        </Card.Body>
                </Card>
            </Tab>
            <Tab eventKey="maven" title="Maven" className="navcard">
                <Card bg="dark" text="white" className="navcard">
                    <Card.Header>Maven repo</Card.Header>
                        <Card.Body>
                            <div>Config</div>
                            <code>
                            &lt;repository&gt;<br/>
                            &lt;id&gt;gozilla&lt;/id&gt;<br/>
                            &lt;name&gt;gozilla&lt;/name&gt;<br/>
                            &lt;url&gt;https://{window.location.hostname}/api/v1/maven/{subject}/{repo}&lt;/url&gt;<br/>
                            &lt;/repository&gt;
                            </code>
                        
                        </Card.Body>
                </Card>
            </Tab>
            <Tab eventKey="conan" title="Conan" className="navcard">
                <Card bg="dark" text="white" className="navcard">
                    <Card.Header>Conan repo</Card.Header>
                        <Card.Body>
                            <Card.Text>
                              Server base URL: https://{window.location.hostname}/api/v1.0/conan
                            </Card.Text>
                        </Card.Body>
                </Card>
            </Tab>
            <Tab eventKey="gitlfs" title="Git LFS" className="navcard">
                <Card bg="dark" text="white" className="navcard">
                    <Card.Header>Git LFS</Card.Header>
                        <Card.Body>
                            <div>Needs Git LFS agent (https://gitlab.inria.fr/osallou/gozilla-git-lfs)</div>)
                            <div>
                                <code>
                                git config --add lfs.customtransfer.goz-lfs.path goz-lfs<br/>
                                git config --add lfs.customtransfer.goz-lfs.args "--subject {subject} --repo {repo} --url https://{window.location.hostname} {data && data.user.authenticated && <span>--user {data.user.id} --apikey <i>your_apikey</i></span>}"<br/>
                                git config --add lfs.standalonetransferagent goz-lfs<br/>
                                </code>
                            </div>
                        </Card.Body>
                </Card>
            </Tab>
            <Tab eventKey="gpg" title="Gpg" className="navcard">
                <Card bg="dark" text="white" className="navcard">
                    <Card.Header>Repo GPG key</Card.Header>
                        <Card.Body>
                            <Card.Text>https://{window.location.hostname}/dl/gpg</Card.Text>
                        </Card.Body>
                </Card>
            </Tab>
            </Tabs>
            </Col>
        </Row>
    )
}

export {GozPackages}

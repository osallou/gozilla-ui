import React, {useState, useEffect} from 'react';
import { Row, Col, Card, Accordion, Button, Form } from 'react-bootstrap';

// import {useUser} from '../user-context'

import "./GozPackages.css";
import { GozMembers } from './GozMembers';

const axios = require('axios');

const GozAdminRepo = ({subject, repo}: any) => {
    // const data:any = useUser();
    const rootUrl = process.env.REACT_APP_GOZ_SERVER ? process.env.REACT_APP_GOZ_SERVER : ""
    const [repoInfo, setRepoInfo] = useState<any|null>(null);
    const [err , setErr] = useState("");
    const [msg , setMsg] = useState("");

    useEffect(() => {
        axios.get(rootUrl + `/api/v1.0/repos/${subject}/${repo}`).then((response: any) => {
            if (response.data.repo.members == null) {
                response.data.repo.members = []
            }
            setRepoInfo(response.data.repo)
        })
    }, [rootUrl, subject, repo]);

    const save = () => {
        axios.patch(rootUrl + `api/v1.0/repos/${subject}/${repo}`, repoInfo).then(() => {
            setMsg("update done!")
            setErr("")
        }).catch(() => { setErr("an error occured")}); setMsg("") ;   }

    const setVisibility = (event: React.FormEvent<HTMLInputElement>) => {
        let s = {...repoInfo}
        s.visibility = event.currentTarget.value
        setRepoInfo(s)
    }

    const update = (field: string) => {
        return (event: React.FormEvent<HTMLInputElement>) => {
            let s = {...repoInfo}
            s[field] = event.currentTarget.value
            setRepoInfo(s)
        }
    }

    const updateMembers = (memberlist: any[]) => {
        let s = {...repoInfo}
        s.members = memberlist
        setRepoInfo(s)
    }

    const profiles = {
        "admin": 10,
        "release manager": 20,
        "developper": 30,
        "guest": 40
    }

    

    return (
<Row>
            {repoInfo &&
            <Col xs={12}>
                {err && <div className="alert alert-warning">{err}</div>}
                {msg && <div className="alert alert-info">{msg}</div>}
                <Card className="navcard">
                    <Card.Header>{repo} [owner: {repoInfo.owner}]</Card.Header>
                    <Card.Body>
                        <Card.Text>{repoInfo.description}</Card.Text>
                    </Card.Body>
                </Card>

                <Accordion defaultActiveKey="0">
                    <Card>
                        <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0">
                            General
                        </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="0">
                        <Card.Body>
                        <Form>
                            <Form.Group controlId="repoForm.Description">
                                <Form.Label>Description</Form.Label>
                                <Form.Control as="textarea" rows="3" type="description" value={repoInfo.description} onChange={update('description')} />
                            </Form.Group>
                            <Form.Group controlId="repoForm.visibility">
                                <Form.Label>Visibility</Form.Label>
                                <Form.Control as="select" value={repoInfo.visibility} onChange={setVisibility}>
                                <option value="0">public</option>
                                <option value="1">protected</option>
                                <option value="2">private</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group controlId="repoForm.save">
                                <Button onClick={save}>Save</Button>
                            </Form.Group>
                        </Form>
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey="1">
                            Members
                        </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="1">
                        <Card.Body>
                            <GozMembers members={repoInfo.members} profiles={profiles} onChange={updateMembers}/>
                            <Form.Group controlId="repoForm.save">
                                <Button onClick={save}>Save</Button>
                            </Form.Group>
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Card.Header className="alert alert-warning">
                        <Accordion.Toggle as={Button} variant="link" eventKey="2">
                        Manage
                        </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="2">
                        <Card.Body>
                            manage visibility, remove, ...
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </Col>
            }
        </Row>
    )
}

export {GozAdminRepo}

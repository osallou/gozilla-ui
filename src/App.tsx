import React, { useState, useEffect } from 'react';
//import logo from './logo.svg';
import './App.css';
import logo from './logo.png';
import {useUser} from './user-context';

import {Container, Row, Col} from 'react-bootstrap'
//const AuthenticatedApp = React.lazy(() => import('./authenticated-app'));
//const UnauthenticatedApp = React.lazy(() => import('./unauthenticated-app'));
//import {AuthenticatedApp} from './authenticated-app';
import UnauthenticatedApp from './unauthenticated-app';

import {Navbar, Nav, NavDropdown, Form, FormControl, Button} from 'react-bootstrap'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faBoxOpen, faServer , faGlobeEurope ,faUser, faFileArchive, faCoffee, faGlobe, faUmbrella, faLock, faAngleDoubleLeft, faAngleDoubleRight, faHome, faSitemap, faArchive, faCodeBranch, faUserShield, faFolderOpen } from '@fortawesome/free-solid-svg-icons'
//import { fab, faJava, faUbuntu, faFedora, faDocker } from '@fortawesome/free-brands-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

//import { BrowserRouter as Router, Switch, Route, Link, Redirect, useParams } from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route, Link,  useLocation, NavLink } from 'react-router-dom';


import {CasCallback} from './auth/CasCallback'
import { GozRepositories } from './components/GozRepositories';
import { GozPackages } from './components/GozPackages';
import { GozSubjects } from './components/GozSubjects';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { GozVersions } from './components/GozVersions';
import { GozDetails } from './components/GozDetails';
//import { GozAdminRepo } from './components/GozAdminRepo';
//import { GozAdminSubject } from './components/GozAdminSubject';
import { GozUser } from './components/GozUser';
import { GozSubjectEdit } from './components/GozSubjectEdit';
import { GozRepoEdit } from './components/GozRepoEdit';
import { GozPackageEdit } from './components/GozPackageEdit';
import { GozPackageVersionEdit } from './components/GozPackageVersionEdit';
import { getSubjectRole, getRepoRole } from './Utils';
import { GozSearch } from './components/GozSearch';
import { createBrowserHistory } from 'history';

//library.add(fab, faCoffee, faGlobe, faUmbrella, faLock, faAngleDoubleLeft, faAngleDoubleRight, faHome, faSitemap, faArchive, faCodeBranch, faUserShield, faFolderOpen, faJava, faUbuntu, faFedora, faDocker)
library.add(fab, faBoxOpen, faServer, faGlobeEurope, faUser, faFileArchive, faCoffee, faGlobe, faUmbrella, faLock, faAngleDoubleLeft, faAngleDoubleRight, faHome, faSitemap, faArchive, faCodeBranch, faUserShield, faFolderOpen)
//const axios = require('axios');


export const history = createBrowserHistory({
  basename: process.env.PUBLIC_URL
});

const IsObjAdmin = (role: number):boolean => {
  if(role > 0 && role <= 10) {
    return true
  }
  return false
}


const IsObjDev = (role: number):boolean => {
  if(role > 0 && role <= 30) {
    return true
  }
  return false
}

const LeftMenu = ({expand}:any) => {
  const data:any = useUser();

  //let {params} = useRouteMatch("/repos/:subject");
  let location = useLocation();
  const [subject, setSubject] = useState('');
  const [repo, setRepo] = useState('');
  const [pack, setPackage] = useState('');
  const [versionpack, setVersionPackage] = useState('');

  const [subjectRole, setSubjectRole] = useState(0)
  const [repoRole, setRepoRole] = useState(0)

  //const [searchRes, setSearchRes] = useState<any[]>([])
  
  useEffect(() => {
    // Met à jour le titre du document via l’API du navigateur
    let pathElts = location.pathname.split('/')
    if (pathElts.length > 0 && pathElts[1] === 'admin'){
      if (pathElts.length > 2) {
        setSubject(pathElts[3])
      }
      if (pathElts.length > 3) {
        setRepo(pathElts[4])
      }
    }
    if (pathElts.length > 0 && pathElts[1] === 'repos'){
      if (pathElts.length > 2) {
        if(pathElts[2] !== subject) {
          getSubjectRole(pathElts[2]).then((role:number) => {setSubjectRole(role);})
        }
        setSubject(pathElts[2])
      }
      if (pathElts.length > 3) {
        if(pathElts[2] !== subject || pathElts[3] !== repo) {
          getRepoRole(pathElts[2], pathElts[3]).then((role:number) => setRepoRole(role))
        }
        setRepo(pathElts[3])
      }
      if (pathElts.length > 4) {
        setPackage(pathElts[4])
      }

      if (pathElts.length > 5) {
        setVersionPackage(pathElts[5])
      }
      
    }
    //console.debug("route changed", location);
  }, [location, repo, subject]); 
  
 
  return (
    <div>
        <div className="menuleft">{expand ? <span><Link to="/repos">Subjects</Link></span>: <Link to="/repos"><FontAwesomeIcon title="subjects" icon="home" size="2x"/></Link>}</div>
        {subject  && <div className="menuleft">{expand ? <span className="menuLeft" ><Link to={`/repos/${subject}`}>Repositories</Link></span>: <Link to={`/repos/${subject}`}><FontAwesomeIcon title="repositories" icon="sitemap" size="2x"/></Link>}</div>}
        {subject && repo && <div className="menuleft">{expand ? <span className="menuLeft"><Link to={`/repos/${subject}/${repo}`}>Packages</Link></span>: <Link to={`/repos/${subject}/${repo}`}><FontAwesomeIcon title="packages" icon="archive" size="2x"/></Link>}</div>}
        {subject && repo && pack && <div className="menuleft">{expand ? <span className="menuLeft"><Link to={`/repos/${subject}/${repo}/${pack}`}>Package versions</Link></span>: <Link to={`/repos/${subject}/${repo}/${pack}`}><FontAwesomeIcon title="Package version" icon="code-branch" size="2x"/></Link>}</div>}
        {data.user.authenticated && subject && IsObjAdmin(subjectRole) && <div className="menuleft">{expand ? <span className="menuLeft"><Link to={`/admin/edit/${subject}`}>Subject admin</Link></span>: <Link to={`/admin/edit/${subject}`}><FontAwesomeIcon title="subject admin" icon="folder-open" size="2x"/></Link>}</div>}
        {data.user.authenticated && subject && repo && (IsObjAdmin(subjectRole) || IsObjAdmin(repoRole)) && <div className="menuleft">{expand ? <span className="menuLeft"><Link to={`/admin/edit/${subject}/${repo}`}>Repository admin</Link></span>: <Link to={`/admin/edit/${subject}/${repo}`}><FontAwesomeIcon title="repo admin" icon="user-shield" size="2x"/></Link>}</div>}
        {data.user.authenticated && subject && repo && pack && (IsObjDev(subjectRole) || IsObjDev(repoRole)) && <div className="menuleft">{expand ? <span className="menuLeft"><Link to={`/admin/edit/${subject}/${repo}/${pack}`}>Package admin</Link></span>: <Link to={`/admin/edit/${subject}/${repo}/${pack}`}><FontAwesomeIcon title="package admin" icon="box-open" size="2x"/></Link>}</div>}
        {data.user.authenticated && subject && repo && pack && versionpack && (IsObjDev(subjectRole) || IsObjDev(repoRole)) && <div className="menuleft">{expand ? <span className="menuLeft"><Link to={`/admin/edit/${subject}/${repo}/${pack}/${versionpack}`}>Version admin</Link></span>: <Link to={`/admin/edit/${subject}/${repo}/${pack}/${versionpack}`}><FontAwesomeIcon title="version admin" icon="server" size="2x"/></Link>}</div>}
    </div>
  )
}


const CreateMenu = ({expand}:any) => {
  //const data:any = useUser();

  //let {params} = useRouteMatch("/repos/:subject");
  let location = useLocation();
  const [subject, setSubject] = useState('');
  const [repo, setRepo] = useState('');
  const [pack, setPackage] = useState('');
  const [subjectRole, setSubjectRole] = useState(0)
  const [repoRole, setRepoRole] = useState(0)
  
  useEffect(() => {
    // Met à jour le titre du document via l’API du navigateur
    let pathElts = location.pathname.split('/')
    if (pathElts.length > 0 && pathElts[1] === 'admin'){
      if (pathElts.length > 2) {
        setSubject(pathElts[3])
      }
      if (pathElts.length > 3) {
        setRepo(pathElts[4])
      }
    }
    if (pathElts.length > 0 && pathElts[1] === 'repos'){
      if (pathElts.length > 2) {
        if(pathElts[2] !== subject) {
          getSubjectRole(pathElts[2]).then((role:number) => {setSubjectRole(role);})
        }
        setSubject(pathElts[2])
      }
      if (pathElts.length > 3) {
        if(pathElts[2] !== subject || pathElts[3] !== repo) {
          getRepoRole(pathElts[2], pathElts[3]).then((role:number) => setRepoRole(role))
        }
        setRepo(pathElts[3])
      }
      if (pathElts.length > 4) {
        setPackage(pathElts[4])
      }
      
    }
    //console.debug("route changed", location);
  }, [location, repo, subject]); 
  
 
  return (
    <NavDropdown  title="Create" id="basic-nav-dropdown">
    <NavDropdown.Item as={NavLink} to="/admin/create">Subject</NavDropdown.Item>
    {subject && (IsObjDev(subjectRole)) && <NavDropdown.Item as={NavLink} to={`/admin/create/${subject}`}>Repo</NavDropdown.Item>}
    {subject && repo && (IsObjDev(subjectRole) || IsObjDev(repoRole)) && <NavDropdown.Item as={NavLink} to={`/admin/create/${subject}/${repo}`}>Package</NavDropdown.Item>}
    {subject && repo && pack && (IsObjDev(subjectRole) || IsObjDev(repoRole)) && <NavDropdown.Item as={NavLink} to={`/admin/create/${subject}/${repo}/${pack}`}>Version</NavDropdown.Item>}
    </NavDropdown>
  )
}

const App: React.FC = () => {
  const data:any = useUser();

  const [expand, setExpand] = useState(false)
  const [search, setSearch] = useState("")

  const onSearchChange = (event: React.FormEvent<HTMLInputElement>) => {
      let s = event.currentTarget.value
      setSearch(s)
  }

  const logout = () => {
    data.logout();
  }

  const toggleExpand = () => {
    setExpand(!expand)
  }

  return (
    <div className="App">
      <Router>
      <Container>
        <Navbar variant="dark" bg="dark" expand="lg">
          <Navbar.Brand href="#"><img className="App-logo" src={logo} alt="gozilla logo"/></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
            <Nav.Link as={NavLink} to="/repos">Home</Nav.Link>
              {data.user.authenticated && <CreateMenu/> }
            </Nav>
            <Form inline>
              <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={onSearchChange} value={search} />
              <Nav.Link as={NavLink} to={{
                pathname: '/search',
                state: {
                  q: search
                }
              }}><Button>Search</Button></Nav.Link>
            </Form>
            {!data.user.authenticated ? <Nav.Link as={NavLink} to="/login">Login</Nav.Link>: ""}
            { data.user.authenticated && <NavDropdown menuRole="user management" title={data.user.id} id="basic-nav-dropdown">
                <NavDropdown.Item as={NavLink} to="/me">Profile</NavDropdown.Item>
                <NavDropdown.Item as={Button} onClick={logout}>Logout</NavDropdown.Item>
              </NavDropdown>
            }
          </Navbar.Collapse>
        </Navbar>
      </Container>

      <Container className="App-body">
        <Row className="auth">
            <Col xs={expand? 3: 1} className="text-left">
              <LeftMenu expand={expand}/>
                <div className="menuleft" onClick={toggleExpand}>{expand? <FontAwesomeIcon icon="angle-double-left" size="2x"/>: <FontAwesomeIcon icon="angle-double-right" size="2x"/>}</div>
            </Col>
            <Col >
              <Switch>
                <Route path="/login" component={UnauthenticatedApp}/>
                <Route path="/search" component={GozSearch} />
                <Route path="/callback/cas/:server" component={CasCallback}/>
                <Route path="/repos/:subject/:repo/:pack/:version" render={(props) => <GozDetails className="navcard"  subject={props.match.params.subject} repo={props.match.params.repo} pack={props.match.params.pack} version={props.match.params.version} />} />
                <Route path="/repos/:subject/:repo/:pack" render={(props) => <GozVersions className="navcard"  subject={props.match.params.subject} repo={props.match.params.repo} pack={props.match.params.pack} />} />
                <Route path="/repos/:subject/:repo" render={(props) => <GozPackages className="navcard"  subject={props.match.params.subject} repo={props.match.params.repo} />} />
                <Route path="/repos/:subject" render={(props) => <GozRepositories className="navcard"  subject={props.match.params.subject} />} />
                <Route path="/admin/edit/:subject/:repo/:pack/:version" render={(props) => <GozPackageVersionEdit subject_id={props.match.params.subject} repo_id={props.match.params.repo} pack_id={props.match.params.pack} pack_vid={props.match.params.version} />} />
                <Route path="/admin/edit/:subject/:repo/:pack" render={(props) => <GozPackageEdit subject_id={props.match.params.subject} repo_id={props.match.params.repo} pack_id={props.match.params.pack} />} />
                <Route path="/admin/edit/:subject/:repo" render={(props) => <GozRepoEdit subject_id={props.match.params.subject} repo_id={props.match.params.repo} />} />
                <Route path="/admin/edit/:subject" render={(props) => <GozSubjectEdit subject_id={props.match.params.subject} />} />
                <Route path="/admin/create/:subject/:repo/:pack" render={(props) => <GozPackageVersionEdit subject_id={props.match.params.subject} repo_id={props.match.params.repo} pack_id={props.match.params.pack} />} />
                <Route path="/admin/create/:subject/:repo" render={(props) => <GozPackageEdit subject_id={props.match.params.subject} repo_id={props.match.params.repo} />} />
                <Route path="/admin/create/:subject" render={(props) => <GozRepoEdit subject_id={props.match.params.subject} />} />
                <Route path="/admin/create" render={(props) => <GozSubjectEdit />} />
                <Route exact path="/repos" render={(props) => <GozSubjects className="navcard" /> }/>
                <Route exact path="/me" render={(props) => <GozUser className="navcard" /> }/>
              </Switch>
          </Col>
        </Row>
      </Container>
      </Router>
    </div>
  );
}

export default App;
